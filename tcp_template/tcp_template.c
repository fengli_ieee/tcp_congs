#include <linux/module.h>
#include <linux/mm.h>
#include <net/tcp.h>
#include <linux/inet_diag.h>
#include <linux/kernel.h>

#define USEC_TO_MSEC 1000

u32 init_cwnd = 10;

struct wrc {
        u32 init_rtt;
        u32 min_rtt;
        u32 max_rtt;
        u32 loss_cwnd;
};

static inline u32 gettime_ms(void)
{
        return ktime_to_ms(ktime_get_real());
}

static inline u32 get_rtt_us(const struct sock *sk)
{
        const struct tcp_sock *tp = tcp_sk(sk);
        const u32 srtt_us = tp->srtt_us >> 3;
        return srtt_us;
}

static inline u32 get_rtt_ms(const struct sock *sk)
{
        const u32 srtt_us = get_rtt_us(sk);
        return (srtt_us / 1000);
}

static void wrc_init(struct sock *sk)
{
        struct wrc *ca = inet_csk_ca(sk);
        ca->min_rtt = TCP_INFINITE_SSTHRESH; // unsigned(-1)
        ca->max_rtt = 0;
        return;
}

static u32 wrc_ssthresh(struct sock *sk)
{
        struct tcp_sock *tp = tcp_sk(sk);
        struct wrc *ca = inet_csk_ca(sk);
        u32 new_ssthresh = (tp->snd_cwnd * 2) / 3;
        ca->loss_cwnd = tp->snd_cwnd;  
        return max(new_ssthresh, init_cwnd);
}

static void wrc_cwnd_event(struct sock *sk, enum tcp_ca_event ev)
{
        struct tcp_sock *tp = tcp_sk(sk);

        switch (ev) {
        case    CA_EVENT_LOSS:
                tp->snd_ssthresh = TCP_INFINITE_SSTHRESH;
                tp->snd_cwnd = init_cwnd;
                break;
        case    CA_EVENT_COMPLETE_CWR:
                if (tp->snd_ssthresh < TCP_INFINITE_SSTHRESH) {
                        tp->snd_cwnd = tp->snd_ssthresh;
                        tp->snd_ssthresh =  TCP_INFINITE_SSTHRESH;
                }
                break;
        default:
                break;
        }

        return;
}

static u32 wrc_cwnd_undo(struct sock *sk)
{
        struct wrc *ca = inet_csk_ca(sk);
	return max(tcp_sk(sk)->snd_cwnd, ca->loss_cwnd);
}


static void wrc_cong_avoid(struct sock *sk, u32 ack, u32 in_flight)
{
        struct tcp_sock *tp = tcp_sk(sk);

        tp->app_limited = (tp->delivered + tcp_packets_in_flight(tp)) ? : 1;


        if (tp->snd_cwnd < tp->snd_ssthresh) {
                // in slow start
                tcp_slow_start(tp, in_flight);
                return;
        }

        tcp_reno_cong_avoid(sk, ack, in_flight);

        return;
}

static inline void wrc_pkts_acked(struct sock *sk, const struct ack_sample *sample)
{
        struct wrc *ca = inet_csk_ca(sk);
        s32  rtt_us = (sample->rtt_us);
        s32  rtt_ms = rtt_us / 1000;

        if (rtt_ms <=0) {
                return;
        }

        if (ca->init_rtt == 0) {
                ca->init_rtt = rtt_ms;
        }

        if (ca->max_rtt < rtt_ms) {
                ca->max_rtt = rtt_ms;
        }

        if (ca->min_rtt > rtt_ms) {
                ca->min_rtt = rtt_ms;
        }

        return;
}

static struct tcp_congestion_ops wrc __read_mostly = {
	.init		= wrc_init,
        .cong_avoid     = wrc_cong_avoid,
	.cwnd_event	= wrc_cwnd_event,
	.ssthresh	= wrc_ssthresh,
        .pkts_acked     = wrc_pkts_acked,
	.undo_cwnd	= wrc_cwnd_undo,
	.flags		= TCP_CONG_NEEDS_ECN,
	.owner		= THIS_MODULE,
	.name		= "template",
};

static int __init wrc_register(void)
{
	BUILD_BUG_ON(sizeof(struct wrc) > ICSK_CA_PRIV_SIZE);
	return tcp_register_congestion_control(&wrc);
}

static void __exit wrc_unregister(void)
{
	tcp_unregister_congestion_control(&wrc);
}

module_init(wrc_register);
module_exit(wrc_unregister);

MODULE_AUTHOR("Daniel Borkmann <dborkman@redhat.com>");

MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("WRC Template");
