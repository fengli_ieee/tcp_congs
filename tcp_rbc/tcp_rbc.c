/* TCP RBC - RTT base Control
*	Based on Congestion avoidance algortihm proposed by Verizon
*	Max and Min congestion window limits based on RTT
*	values. Two RTT markers ties the values of Max and
*	Min Cwnd. In between RTT values, calculate the Cwnd based on
*	slope between Max and Min Cwnd values.
*	Please refer "Requirements for New TCP Congestion Avoidance
	Module" doc, by Vz Labs

*	Assumption - Since congestion window operates on MSS,
*	all cwnd calculations are in terms of segments.
*
*	Module Parameters
*  	cwnd_highbound - Maximum possible value of Cwnd in Bytes
	cwnd_lowbound - Maximum value of Cwnd (Bytes) when RTT crosses high mark
*	rtt_lowvaluemarker(ms)-RTT marker below which Cwnd stays at cwnd_highbound
*	rtt_highvaluemarke(ms)-RTT marker after which Cwnd stays at cwnd_lowbound
*
*	Global Variables
*	maxcwnd_rttslope - Calculated slope of max Cwnd decline between
*	rtt_lowvaluemarker and rtt_highvaluemarker
*	tcp_rbc_lpf_rtt_thresh(ms) - High RTT values above this value
*	tcp_rbc_rtt_ewma_weight	- Weight for EWMA RTT
*   	tcp_rbc_rtt_ewma_light_weight - Weight for using High RTT values
*
*	Important Functions
*	tcp_rbc_maxcwndrttslope(void)-slope of max cwnd with rtti.e. y2-y1/x2-x1
*
*	get_cwnd_inbound_sgmnts(u32 crntrtt, u32 mss) - get cwnd max size when
*	rtt_lowvaluemarker<RTT<rtt_highvaluemarker. Called each time RTT value
* 	is re-calculated
*
*	tcp_rbc_lpf_srtt(s32 rtt_ms, u32 srtt_prev) - Verizon version of
*	calculating RTT. Called upon rx of ack. Follow EWMA formula
* 	  rtt =  prev_rtt*(1-w) + crnt_rtt*w
*	value of w can be 1,3, or 12 dependening upon crnt_rtt
*/

#include <net/tcp.h>
#include <linux/module.h>
#include <linux/mm.h>
#include <linux/types.h>
#include <linux/list.h>
#include <linux/kernel.h>
#include <linux/version.h>

static u32 cwnd_highbound = 1048576;
static u32 cwnd_lowbound = 524288;
static u32 rtt_lowvaluemarker = 100;
static u32 rtt_highvaluemarker = 500;
static int maxcwnd_rttslope;
static u32 tcp_rbc_lpf_rtt_thresh = 700;
static u32 tcp_rbc_rtt_ewma_weight = 3;
static u32 tcp_rbc_rtt_ewma_light_weight = 1;
static u32 tcp_rbc_rtt_ewma_heavy_weight = 12;
static u32 tcp_rbc_rtt_max_weight = 100;
static bool rbc_debug = 0;

module_param(cwnd_highbound, uint, 0644);
MODULE_PARM_DESC(cwnd_highbound, " max upper bound of cwnd in bytes");
module_param(cwnd_lowbound, uint, 0644);
MODULE_PARM_DESC(cwnd_lowbound, " max value of cwnd when rtt > 500ms");
module_param(rtt_lowvaluemarker, uint, 0644);
MODULE_PARM_DESC(rtt_lowvaluemarker, " low value marker for rtt");
module_param(rtt_highvaluemarker, uint, 0644);
MODULE_PARM_DESC(rtt_highvaluemarker, " high value marker for rtt");
module_param(rbc_debug, bool, 0644);
MODULE_PARM_DESC(rbc_debug, " enable/disable debug logs");

struct rbctcp {
	u32 maxcwnd;
	u32 mincwnd;
	u32 srtt;
	s64 start_tm;
};

static inline int tcp_rbc_maxcwndrttslope(void)
{
	int ydiff = cwnd_lowbound - cwnd_highbound;
	int xdiff = rtt_highvaluemarker - rtt_lowvaluemarker;

	return (ydiff / xdiff);
}

static inline u32 get_cwnd_ubound_sgmnts(u32 mss)
{
	return (cwnd_highbound / mss);

}

static inline u32 get_cwnd_lbound_sgmnts(u32 mss)
{
	return (cwnd_lowbound / mss);
}

static u32 get_cwnd_inbound_sgmnts(u32 crntrtt, u32 mss)
{
	if (crntrtt >= rtt_highvaluemarker)
		return get_cwnd_lbound_sgmnts(mss);
	else if (crntrtt <= rtt_lowvaluemarker)
		return get_cwnd_ubound_sgmnts(mss);
	else {
		u32 crntcwnd_bytes =
		    cwnd_highbound +
		    ((crntrtt - rtt_lowvaluemarker) * maxcwnd_rttslope);

		return (crntcwnd_bytes / mss);
	}
}

static inline s64 gettime_us(void)
{
	return ktime_to_us(ktime_get_real());
}

static void tcp_rbc_reset(struct rbctcp *ca, u32 mss)
{
	ca->maxcwnd = get_cwnd_ubound_sgmnts(mss);
	ca->mincwnd = get_cwnd_lbound_sgmnts(mss);
}

static void tcp_rbc_init(struct sock *sk)
{
	struct rbctcp *ca = inet_csk_ca(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	tcp_rbc_reset(ca, tp->mss_cache);
	ca->start_tm = gettime_us();

	if ((cwnd_highbound <= cwnd_lowbound)
	    || (rtt_highvaluemarker <= rtt_lowvaluemarker))
		maxcwnd_rttslope = 0;
	else if ((cwnd_highbound > cwnd_lowbound) &&
		 (rtt_highvaluemarker > rtt_lowvaluemarker)) {
		maxcwnd_rttslope = tcp_rbc_maxcwndrttslope();
	}
	if (rbc_debug)
		printk(KERN_INFO
		       "RBC TCP init, start_tm(us): %lld, cwnd_highbound: %u, cwnd_lowbound: %u, rtt_highmarker: %u, rtt_lowvaluemarker: %u, maxcwnd_rttslope: %d\n",
		       ca->start_tm, cwnd_highbound, cwnd_lowbound,
		       rtt_highvaluemarker, rtt_lowvaluemarker,
		       maxcwnd_rttslope);

	tp->snd_cwnd = 1;
	tp->snd_cwnd_clamp = get_cwnd_ubound_sgmnts(tp->mss_cache);
	if (rbc_debug)
		printk(KERN_INFO
		       "RBC TCP Initalized, mss: %u, maxwind: %u, cwnd: %u\n",
		       tp->mss_cache, tp->snd_cwnd_clamp, tp->snd_cwnd);
}

static void tcp_rbc_cong_avoid(struct sock *sk, u32 ack, u32 in_flight)
{
	struct tcp_sock *tp = tcp_sk(sk);

#if LINUX_VERSION_CODE <= KERNEL_VERSION(3,15,0)
	if (!tcp_is_cwnd_limited(sk, in_flight))
		return;
#else
	if (!tcp_is_cwnd_limited(sk))
		return;
#endif

	if (tp->snd_cwnd < tp->snd_ssthresh) {
#if LINUX_VERSION_CODE <= KERNEL_VERSION(3,13,0)
		tcp_slow_start(tp);
#else
		tcp_slow_start(tp, in_flight);
#endif
	}
}

static inline u32 tcp_rbc_ssthresh(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	u32 newssthresh = (tp->snd_cwnd * 2) / 3;
	return max(newssthresh, 2U);
}

static inline u32 tcp_rbc_lpf_srtt(s32 rtt_ms, u32 srtt_prev)
{

	u32 srtt_crnt = srtt_prev;
	u32 maxweight = tcp_rbc_rtt_max_weight;

	if (rtt_ms <= 0) {
		return srtt_crnt;
	}

	if (rtt_ms > tcp_rbc_lpf_rtt_thresh) {
		u32 weight = tcp_rbc_rtt_ewma_light_weight;
		srtt_crnt =
		    (srtt_prev * (maxweight - weight) +
		     rtt_ms * weight) / maxweight;
		return srtt_crnt;
	}

	if (srtt_prev < rtt_lowvaluemarker) {
		u32 weight = tcp_rbc_rtt_ewma_heavy_weight;
		srtt_crnt =
		    (srtt_prev * (maxweight - weight) +
		     rtt_ms * weight) / maxweight;
		return srtt_crnt;
	}

	if ((srtt_prev >= rtt_lowvaluemarker)
	    && (srtt_prev < rtt_highvaluemarker)) {
		u32 weight = tcp_rbc_rtt_ewma_weight;
		srtt_crnt =
		    (srtt_prev * (maxweight - weight) +
		     rtt_ms * weight) / maxweight;
		return srtt_crnt;
	}

	if (srtt_prev >= rtt_highvaluemarker) {
		u32 weight = tcp_rbc_rtt_ewma_heavy_weight;
		srtt_crnt =
		    (srtt_prev * (maxweight - weight) +
		     rtt_ms * weight) / maxweight;
		return srtt_crnt;
	}

	return srtt_crnt;
}

static void tcp_rbc_pkts_acked(struct sock *sk, u32 cnt, s32 rtt_us)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct rbctcp *ca = inet_csk_ca(sk);
	u32 srtt_crnt = 0;
	if (rtt_us > 0) {
		s32 rtt_ms = rtt_us / 1000;
		u32 srtt_prev = ca->srtt;
		srtt_crnt = tcp_rbc_lpf_srtt(rtt_ms, srtt_prev);
		if (srtt_crnt > 0)
			ca->srtt = srtt_crnt;
		else
			ca->srtt = rtt_ms;

		tp->snd_cwnd_clamp =
		    get_cwnd_inbound_sgmnts(ca->srtt, tp->mss_cache);
	}
	if (rbc_debug)
		printk(KERN_INFO
		       "RBC TCP pkts acked, elapsed time(us) %lld, rtt: %dusec, vz_srtt: %ums, maxcwnd: %u, send window: %u, count: %u, pkts inflight %u\n",
		       (gettime_us() - ca->start_tm), rtt_us, ca->srtt,
		       tp->snd_cwnd_clamp, tp->snd_cwnd, cnt, tp->packets_out);

}

static void tcp_rbc_event(struct sock *sk, enum tcp_ca_event event)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct rbctcp *ca = inet_csk_ca(sk);

	switch (event) {
	case CA_EVENT_LOSS:{
			if (rbc_debug)
				printk(KERN_INFO
				       "RBC TCP LOSS EVENT, elapsed time(us): %lld, send window: %d\n",
				       (gettime_us() - ca->start_tm),
				       tp->snd_cwnd);

			tp->snd_ssthresh = TCP_INFINITE_SSTHRESH;
			tp->snd_cwnd = 1;
			break;
		}
	case CA_EVENT_COMPLETE_CWR:{
			if (tp->snd_ssthresh < TCP_INFINITE_SSTHRESH) {
				tp->snd_cwnd = tp->snd_ssthresh;
				tp->snd_ssthresh = TCP_INFINITE_SSTHRESH;
			}
			if (rbc_debug)
				printk(KERN_INFO
				       "RBC TCP CWR EVENT,elapsed time(us): %lld, send window: %d\n",
				       (gettime_us() - ca->start_tm),
				       tp->snd_cwnd);

			break;
		}
	default:
		break;
	}

}

static struct tcp_congestion_ops tcp_rbc __read_mostly = {
	.init = tcp_rbc_init,
	.ssthresh = tcp_rbc_ssthresh,
	.cong_avoid = tcp_rbc_cong_avoid,
	.pkts_acked = tcp_rbc_pkts_acked,
	.cwnd_event = tcp_rbc_event,
	.owner = THIS_MODULE,
	.name = "rbc"
};

static int __init tcp_rbc_register(void)
{
	BUILD_BUG_ON(sizeof(struct rbctcp) > ICSK_CA_PRIV_SIZE);

	maxcwnd_rttslope = tcp_rbc_maxcwndrttslope();
	printk(KERN_INFO "TCP RBC Register, slope %d\n", maxcwnd_rttslope);
	return tcp_register_congestion_control(&tcp_rbc);
}

static void __exit tcp_rbc_unregister(void)
{
	tcp_unregister_congestion_control(&tcp_rbc);
	printk(KERN_INFO "TCP RBC UnRegistered\n");
}

module_init(tcp_rbc_register);
module_exit(tcp_rbc_unregister);

MODULE_AUTHOR("RBC/Verizon");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("RTT based control TCP");
